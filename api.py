import requests
import hashlib
import datetime


class Session():
    url = 'https://gateway.marvel.com/v1/public/'
    
    def __init__(self, public_key, private_key):
        self.public_key = public_key
        self.private_key = private_key
    
    def call_api(self, endpoint, params={}):
        # get the dictionary of parameters to pass the url
        if params is None:
            params = {}
        
        # Get the god damn hash that they so much want! 
        now = datetime.datetime.now().strftime('%Y-%m-%d%H:%M:%S')
        _hash = hashlib.md5()
        _hash.update(now.encode('utf-8')) # update with the right encoding 
        _hash.update(self.private_key.encode('utf-8'))
        _hash.update(self.public_key.encode('utf-8'))
        
        # add the parameters to the dictionary
        params['hash'] = _hash.hexdigest()
        params['apikey'] = self.public_key
        params['ts'] = now
        
        # generate the final url
        _url = self.url + endpoint
        
        # get the data
        r = requests.get(_url, params=params)
        return r 
